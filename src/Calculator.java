import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введіть перше число:");
        int num1 = scanner.nextInt();

        System.out.println("Введіть операцію (+, -, *, /, НОД, НОК, КОРЕНІ):");
        String operation = scanner.next();

        if (operation.equalsIgnoreCase("НОД")) {
            System.out.println("Введіть друге число:");
            int num2 = scanner.nextInt();
            int gcd = findGCD(num1, num2);
            System.out.println("НОД: " + gcd);
        } else if (operation.equalsIgnoreCase("НОК")) {
            System.out.println("Введіть друге число:");
            int num2 = scanner.nextInt();
            int lcm = findLCM(num1, num2);
            System.out.println("НОК: " + lcm);
        } else if (operation.equalsIgnoreCase("КОРЕНІ")) {
            System.out.println("Введіть друге число:");
            int num2 = scanner.nextInt();
            System.out.println("Введіть третє число:");
            int num3 = scanner.nextInt();
            double discriminant = num2 * num2 - 4 * num1 * num3;

            if (discriminant < 0) {
                throw new ArithmeticException("Error: Quadratic equation has no real roots.");
            }

            if (discriminant == 0) {
                double root = -num2 / (2 * num1);
                System.out.print("x = " + root);
            } else if (discriminant > 0) {
                double root1 = (-num2 + Math.sqrt(discriminant)) / (2 * num1);
                double root2 = (-num2 - Math.sqrt(discriminant)) / (2 * num1);

                System.out.print("x1 = " + root1 + "\nx2 = " + root2);
            }

        } else {
            System.out.println("Введіть друге число:");
            int num2 = scanner.nextInt();
            double result;

            switch (operation) {
                case "+":
                    result = num1 + num2;
                    System.out.println("Результат: " + result);
                    break;
                case "-":
                    result = num1 - num2;
                    System.out.println("Результат: " + result);
                    break;
                case "*":
                    result = num1 * num2;
                    System.out.println("Результат: " + result);
                    break;
                case "/":
                    if (num2 != 0) {
                        result = (double) num1 / num2;
                        System.out.println("Результат: " + result);
                    } else {
                        System.out.println("Помилка: ділення на нуль");
                    }
                    break;
                default:
                    System.out.println("Неправильна операція");
            }
        }
    }

    // Функція для обчислення найбільшого спільного дільника (НСД)
    public static int findGCD(int a, int b) {
        if (b == 0)
            return a;
        return findGCD(b, a % b);
    }

    // Функція для обчислення найменшого спільного кратного (НСК)
    public static int findLCM(int a, int b) {
        return (a * b) / findGCD(a, b);
    }
}
