# Лабораторна робота №1
___

**Тема:** Дослідження систем контроля версій GIT та налагодження оточення Java/Kotlin розробника.

**Мета:** Навчитися налагоджувати оточення Java/Kotlin-розробника. Вивчити основи використання системи GIT.

## Хід роботи
**Завдання 0 (обов’язкове)**

Пройти тренінг: http://learngitbranching.js.org/.

Результати виконання:
1. Розділ **MAIN**

![1.png](images%2F1.png)
![2.png](images%2F2.png)
![3.png](images%2F3.png)
![4.png](images%2F4.png)
![5.png](images%2F5.png)
![6.png](images%2F6.png)
![7.png](images%2F7.png)
![8.png](images%2F8.png)
![9.png](images%2F9.png)
![10.png](images%2F10.png)
![11.png](images%2F11.png)
![12.png](images%2F12.png)
![13.png](images%2F13.png)
![14.png](images%2F14.png)
![15.png](images%2F15.png)
![16.png](images%2F16.png)
![17.png](images%2F17.png)
![18.png](images%2F18.png)
2. Розділ **REMOTE**

![19.png](images%2F19.png)
![20.png](images%2F20.png)
![21.png](images%2F21.png)
![22.png](images%2F22.png)
![23.png](images%2F23.png)
![24.png](images%2F24.png)
![25.png](images%2F25.png)
![26.png](images%2F26.png)
![27.png](images%2F27.png)
![28.png](images%2F28.png)
![29.png](images%2F29.png)
![30.png](images%2F30.png)
![31.png](images%2F31.png)
![32.png](images%2F32.png)
![33.png](images%2F33.png)

**Результати**

![34.png](images%2F34.png)
![35.png](images%2F35.png)

### **Завдання 2 (командне 75-100%)** 

### 1. Перевіряємо версію Git, задане ім'я та пошту користувача

![37.png](images%2F37.png)

### 2. Виконуємо ініціалізацію директорії

![38.png](images%2F38.png)

### 3. Виконуємо перший коміт

![39.png](images%2F39.png)

### 4. Додаємо зв'язок з віддаленим репозиторієм та виконуємо пуш

![40.png](images%2F40.png)


### 5. Створюємо нову гілку **"addition"**, виконуємо коміт і мердж

![41.png](images%2F41.png)

### 6. Виконуємо останній пуш 

![42.png](images%2F42.png)

### 7. Запушений проєкт на Гітлаб

![51.png](images%2F51.png)

### 8. Додаємо сокомандника

![43.png](images%2F43.png)
![44.png](images%2F44.png)

### 9. Історія проєкту

![46.png](images%2F46.png)

### 10. Приклад роботи калькулятора

### **Операція додавання:**

![47.png](images%2F47.png)

### **Знаходження НОД**

![48.png](images%2F48.png)

### **Знаходження НОК**

![49.png](images%2F49.png)

### **Знаходження коренів квадратного рівняння**

![50.png](images%2F50.png)


**ВИСНОВОК:** навчилася налагоджувати оточення Java/Kotlin-розробника.
Вивчила основи використання системи GIT.
